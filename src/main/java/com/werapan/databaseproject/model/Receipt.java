/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.MemberDao;
import com.werapan.databaseproject.dao.MemberDao;
import com.werapan.databaseproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Receipt {

    private int id;
    private Date createdDate;
    private float total;
    private float cash;
    private int totalQty;
    private int userId;
    private int memberId;
    private User user;
    private Member member;
    private ArrayList<ReceiptDetail> receiptDetails = new ArrayList<ReceiptDetail>();

    public Receipt(int id, Date createdDate, float total, float cash, int totalQty, int userId, int memberId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.memberId = memberId;
    }

    public Receipt(float total, float cash, int totalQty, int userId, int memberId) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.memberId = memberId;
    }

    public Receipt(float cash, int userId, int memberId) {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = cash;
        this.totalQty = 0;
        this.userId = userId;
        this.memberId = memberId;
    }
    
    public Receipt() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = 0;
        this.totalQty = 0;
        this.userId = 0;
        this.memberId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
        this.memberId = member.getId();
    }

    public ArrayList<ReceiptDetail> getReceiptDeatails() {
        return receiptDetails;
    }

    public void setReceiptDeatails(ArrayList receiptDeatails) {
        this.receiptDetails = receiptDeatails;
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", cash=" + cash + ", totalQty=" + totalQty + ", userId=" + userId + ", memberId=" + memberId + ", user=" + user + ", member=" + member + ", receiptDeatails=" + receiptDetails + '}';
    }

    
public void addReciptDetail(ReceiptDetail receiptDetail){
    receiptDetails.add(receiptDetail);
    calculateTotal();
}
public void addReciptDetail(Product product,int qty){
    ReceiptDetail rd = new ReceiptDetail(product.getId(),product.getName(),product.getPrice(),qty,qty*product.getPrice(),-1);
    receiptDetails.add(rd);
    calculateTotal();
}

public void delReciptDetail(ReceiptDetail receiptDetail){
    receiptDetails.remove(receiptDetail);
    calculateTotal();
}


public void calculateTotal(){
    int totalQty =0;
    float total = 0.0f;
    for(ReceiptDetail rd : receiptDetails){
        total+= rd.getTotalPrice();
        totalQty += rd.getQty();
    }
    this.totalQty = totalQty;
    this.total =total;
}

    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("receipt_id"));
            receipt.setCreatedDate(rs.getTimestamp("created_date"));
            receipt.setTotal(rs.getFloat("total"));
            receipt.setCash(rs.getFloat("cash"));
            receipt.setTotalQty(rs.getInt("total_qty"));
            receipt.setUserId(rs.getInt("user_id"));
            receipt.setMemberId(rs.getInt("cus_id"));
            
            //Population
            MemberDao memberDao = new MemberDao();
            UserDao userDao = new UserDao();
            Member member = memberDao.get(receipt.getMemberId());
            User user = userDao.get(receipt.getUserId());
            receipt.setMember(member);
            receipt.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receipt;
    }
}